from flask import Flask
from numpy import array
from keras.preprocessing.text import Tokenizer
from keras.utils import to_categorical
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Embedding
from pickle import dump
from flask_cors import CORS

#Global variables
model_buffer = ' '
tokenizer = Tokenizer()
model = Sequential()
max_length = 0

# generate a sequence from a language model

def generate_seq(model, tokenizer, max_length, seed_text, n_words):
	in_text = seed_text
	# generate a fixed number of words
	for _ in range(n_words):
		# encode the text as integer
		encoded = tokenizer.texts_to_sequences([in_text])[0]
		# pre-pad sequences to a fixed length
		encoded = pad_sequences([encoded], maxlen=max_length, padding='pre')
		# predict probabilities for each word
		yhat = model.predict_classes(encoded, verbose=0)
		# map predicted word index to word
		out_word = ''
		for word, index in tokenizer.word_index.items():
			if index == yhat:
				out_word = word				
				break
		# append to input
		in_text += ' ' + out_word
	return in_text

# load doc into memory
def load_doc(filename):
	# open the file as read only
	file = open(filename, 'r')  
	# read all text
	text = file.read()
	# close the file
	file.close()
	return text

def predict_word(context, words):
         return (generate_seq(model, tokenizer, max_length-1, context, words))

def prepare():
                 # source text
         data = load_doc(r'hr.txt')
         global model_buffer
         global tokenizer
         global model
         global max_length
         # prepare the tokenizer on the source text
         tokenizer.fit_on_texts([data])
         # determine the vocabulary size
         vocab_size = len(tokenizer.word_index) + 1
         # create line-based sequences
         sequences = list()
         for line in data.split('\n'):
                 encoded = tokenizer.texts_to_sequences([line])[0]
                 for i in range(1, len(encoded)):
                         sequence = encoded[:i+1]
                         sequences.append(sequence)

         # pad input sequences
         max_length = max([len(seq) for seq in sequences])
         from keras.models import load_model
         model = load_model('hr.h5')
         model_buffer = 'X'
         return model, tokenizer, max_length

app = Flask(__name__)
CORS(app)

@app.route('/<string:context>/<int:no_w>' , methods=['GET'])
def predict(context,no_w):
    global model_buffer
    if model_buffer == ' ':
      prepare()
    return (predict_word(context,no_w))

if __name__ == '__main__':

   app.run(host='0.0.0.0', port=8080, debug=True)
